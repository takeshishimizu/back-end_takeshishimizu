﻿CREATE TABLE [dbo].[CustomerSupplierXref] (
    [CustomerCompanyId] INT           NOT NULL,
    [SupplierCompanyId] INT           NOT NULL,
    [SupplierCodes]     VARCHAR (400) NULL,
    CONSTRAINT [PK_CustomerSupplierXref] PRIMARY KEY CLUSTERED ([CustomerCompanyId] ASC, [SupplierCompanyId] ASC),
    CONSTRAINT [FK_CustomerSupplierXref_Customer] FOREIGN KEY ([CustomerCompanyId]) REFERENCES [dbo].[Companies] ([Id]),
    CONSTRAINT [FK_CustomerSupplierXref_Supplier] FOREIGN KEY ([SupplierCompanyId]) REFERENCES [dbo].[Companies] ([Id])
);

