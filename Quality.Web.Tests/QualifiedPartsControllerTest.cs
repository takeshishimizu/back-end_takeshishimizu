﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quality.Web.Controllers;

namespace Quality.Web.Tests
{
    [TestClass]
    public class QualifiedPartsControllerTest
    {
        [TestMethod]
        public void TestQualifiedPartsControllerConstructor()
        {
            var controller = new QualifiedPartsController();
            Assert.IsNotNull(controller);
        }
    }
}
