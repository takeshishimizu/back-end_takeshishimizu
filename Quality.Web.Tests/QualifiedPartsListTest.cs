﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Quality.Web.Models;

namespace Quality.Web.Tests
{
    public class MockDataContext : BaseContext
    {
        private IDataReader CreateDataReader()
        {
            var hasRow = true;
            var reader = new Mock<IDataReader>();
            // Ensure single row in mock
            reader.Setup(x => x.Read())
                .Returns(() => hasRow)
                .Callback(() => hasRow = false);
            // Filling up columns
            reader.Setup(x => x[nameof(QualifiedPart.QPLflag)]).Returns(true);
            reader.Setup(x => x.GetName(0)).Returns(nameof(QualifiedPart.QPLflag));
            reader.Setup(x => x[nameof(QualifiedPart.PartNumber)]).Returns("1234");
            reader.Setup(x => x.GetName(1)).Returns(nameof(QualifiedPart.PartNumber));
            reader.Setup(x => x[nameof(QualifiedPart.PartName)]).Returns("ABC");
            reader.Setup(x => x.GetName(2)).Returns(nameof(QualifiedPart.PartName));
            reader.Setup(x => x[nameof(QualifiedPart.Revision)]).Returns("2");
            reader.Setup(x => x.GetName(3)).Returns(nameof(QualifiedPart.Revision));
            reader.Setup(x => x[nameof(QualifiedPart.ToolDieSetNumber)]).Returns("D114");
            reader.Setup(x => x.GetName(4)).Returns(nameof(QualifiedPart.ToolDieSetNumber));
            reader.Setup(x => x[nameof(QualifiedPart.OpenPOflag)]).Returns(true);
            reader.Setup(x => x.GetName(5)).Returns(nameof(QualifiedPart.OpenPOflag));
            reader.Setup(x => x[nameof(QualifiedPart.Supplier)]).Returns("Sssss");
            reader.Setup(x => x.GetName(6)).Returns(nameof(QualifiedPart.Supplier));
            reader.Setup(x => x[nameof(QualifiedPart.SupplierCode)]).Returns("SC444");
            reader.Setup(x => x.GetName(7)).Returns(nameof(QualifiedPart.SupplierCode));
            reader.Setup(x => x[nameof(QualifiedPart.Jurisdiction)]).Returns("EDCRFV");
            reader.Setup(x => x.GetName(8)).Returns(nameof(QualifiedPart.Jurisdiction));
            reader.Setup(x => x[nameof(QualifiedPart.Classification)]).Returns("CRTYU");
            reader.Setup(x => x.GetName(9)).Returns(nameof(QualifiedPart.Classification));
            reader.Setup(x => x[nameof(QualifiedPart.CTQflag)]).Returns(false);
            reader.Setup(x => x.GetName(10)).Returns(nameof(QualifiedPart.CTQflag));
            reader.Setup(x => x[nameof(QualifiedPart.QPLCreatedByUserId)]).Returns(12345);
            reader.Setup(x => x.GetName(11)).Returns(nameof(QualifiedPart.QPLCreatedByUserId));
            reader.Setup(x => x[nameof(QualifiedPart.QPLCreatedDate)]).Returns(Convert.ToDateTime("2018-7-20"));
            reader.Setup(x => x.GetName(12)).Returns(nameof(QualifiedPart.QPLCreatedDate));
            reader.Setup(x => x[nameof(QualifiedPart.QPLLastUpdatedByUserId)]).Returns(7890);
            reader.Setup(x => x.GetName(13)).Returns(nameof(QualifiedPart.QPLLastUpdatedByUserId));
            reader.Setup(x => x[nameof(QualifiedPart.QPLLastUpdatedDate)]).Returns(Convert.ToDateTime("2018-8-11"));
            reader.Setup(x => x.GetName(14)).Returns(nameof(QualifiedPart.QPLLastUpdatedDate));
            reader.Setup(x => x[nameof(QualifiedPart.QPLExpiresDate)]).Returns(Convert.ToDateTime("2019-9-20"));
            reader.Setup(x => x.GetName(15)).Returns(nameof(QualifiedPart.QPLExpiresDate));
            reader.Setup(x => x.FieldCount).Returns(16);
            return reader.Object;
        }

        public override IDbCommand GetCommand(string cmdText)
        {
            var command = new Mock<IDbCommand>();
            command.Setup(x => x.ExecuteReader()).Returns(CreateDataReader());
            return command.Object;
        }

        public override IDbConnection GetConnection()
        {
            throw new System.NotImplementedException();
        }
    }

    [TestClass]
    public class QualifiedPartsListTest
    {
        [TestMethod]
        public void TestQualifiedPartsList()
        {
            var qpl = new QualifiedPartsList(new MockDataContext());
            Assert.AreEqual(1, qpl.QualifiedParts.Length);

            var qualifiedPart = qpl.QualifiedParts[0];
            Assert.IsTrue(qualifiedPart.QPLflag);
            Assert.AreEqual("1234", qualifiedPart.PartNumber);
            Assert.AreEqual("ABC", qualifiedPart.PartName);
            Assert.AreEqual("2", qualifiedPart.Revision);
            Assert.AreEqual("D114", qualifiedPart.ToolDieSetNumber);
            Assert.IsTrue(qualifiedPart.OpenPOflag);
            Assert.AreEqual("Sssss", qualifiedPart.Supplier);
            Assert.AreEqual("SC444", qualifiedPart.SupplierCode);
            Assert.AreEqual("EDCRFV", qualifiedPart.Jurisdiction);
            Assert.AreEqual("CRTYU", qualifiedPart.Classification);
            Assert.IsFalse(qualifiedPart.CTQflag);
            Assert.AreEqual(12345, qualifiedPart.QPLCreatedByUserId);
            Assert.AreEqual(Convert.ToDateTime("2018-7-20"), qualifiedPart.QPLCreatedDate);
            Assert.AreEqual(7890, qualifiedPart.QPLLastUpdatedByUserId);
            Assert.AreEqual(Convert.ToDateTime("2018-8-11"), qualifiedPart.QPLLastUpdatedDate);
            Assert.AreEqual(Convert.ToDateTime("2019-9-20"), qualifiedPart.QPLExpiresDate);
        }
    }
}
