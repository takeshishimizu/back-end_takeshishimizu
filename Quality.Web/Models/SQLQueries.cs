﻿namespace Quality.Web.Models
{
    public class SqlQueries
    {
        /// <summary>
        /// SQL string to query QualifiedPartsLists table to create QualifiedPartsList
        /// </summary>
        public static string QualifiedPartsListQuery => @"
SELECT
	IsQualified as QPLflag,
	PartNumber,
	PartName,
	Revision,
	ToolDieSetNumber,
	OpenPo as OpenPOflag,
	CompanyName as Supplier,
	SupplierCodes as SupplierCode,
	Jurisdiction,
	Classification,
	Ctq as CTQflag,
	CreatedById as QPLCreatedByUserId,
	CreatedDateUtc as QPLCreatedDate,
	LastUpdatedById as QPLLastUpdatedByUserId,
	LastupdatedDateUtc as QPLLastUpdatedDate,
	ExpirationDateUtc as QPLExpiresDate
FROM QualifiedPartsLists as QPL
INNER JOIN Parts as P
	ON QPL.PartId = P.PartId
INNER JOIN Companies as C
	ON QPL.SupplierCompanyId = C.Id
INNER JOIN CustomerSupplierXref as CSX
	ON QPL.SupplierCompanyId = CSX.SupplierCompanyId
";
    }
}