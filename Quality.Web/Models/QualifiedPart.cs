﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.Serialization;

namespace Quality.Web.Models
{
    /// <summary>
    /// Maps SQL Data Row to QualifiedPart
    /// </summary>
    /// <note>
    /// Expects column mane and property name matches to enforce consistency with SQL query result and object.
    /// </note>
    [DataContract(Name = nameof(QualifiedPart))]
    public class QualifiedPart
    {
        private void SetPropertyValue(string propertyName, IDataRecord reader)
        {
            var property = this.GetType().GetProperty(propertyName);
            if (property == null) throw new ArgumentOutOfRangeException();
            if (property.PropertyType == typeof(string))
            {
                property.SetValue(this, reader[propertyName].ToString());
            }
            else if (property.PropertyType == typeof(DateTime) ||
                     property.PropertyType == typeof(DateTime?))
            {
                if (reader[propertyName] != DBNull.Value)
                {
                    property.SetValue(this, Convert.ToDateTime(reader[propertyName]));
                }
            }
            else if (property.PropertyType == typeof(bool))
            {
                property.SetValue(this, Convert.ToBoolean(reader[propertyName]));
            }
            else
            {
                property.SetValue(this, Convert.ToInt32(reader[propertyName]));
            }
        }

        public QualifiedPart(IDataReader reader)
        {
            if (reader == null) throw new ArgumentNullException();
            for (var index = 0; index < reader.FieldCount; index++)
            {
                SetPropertyValue(reader.GetName(index), reader);
            }
        }

        [Display(Name = nameof(QPLflag))]
        [DataMember(Name = nameof(QPLflag))]
        public bool QPLflag { get; set; }

        [Display(Name = nameof(PartNumber))]
        [DataMember(Name = nameof(PartNumber))]
        public string PartNumber { get; set; }

        [Display(Name = nameof(PartName))]
        [DataMember(Name = nameof(PartName))]
        public string PartName { get; set; }

        [Display(Name = nameof(Revision))]
        [DataMember(Name = nameof(Revision))]
        public string Revision { get; set; }

        [Display(Name = nameof(ToolDieSetNumber))]
        [DataMember(Name = nameof(ToolDieSetNumber))]
        public string ToolDieSetNumber { get; set; }

        [Display(Name = nameof(OpenPOflag))]
        [DataMember(Name = nameof(OpenPOflag))]
        public bool OpenPOflag { get; set; }

        [Display(Name = nameof(Supplier))]
        [DataMember(Name = nameof(Supplier))]
        public string Supplier { get; set; }

        [Display(Name = nameof(SupplierCode))]
        [DataMember(Name = nameof(SupplierCode))]
        public string SupplierCode { get; set; }

        [Display(Name = nameof(Jurisdiction))]
        [DataMember(Name = nameof(Jurisdiction))]
        public string Jurisdiction { get; set; }

        [Display(Name = nameof(Classification))]
        [DataMember(Name = nameof(Classification))]
        public string Classification { get; set; }

        [Display(Name = nameof(CTQflag))]
        [DataMember(Name = nameof(CTQflag))]
        public bool CTQflag { get; set; }

        [Display(Name = nameof(QPLCreatedByUserId))]
        [DataMember(Name = nameof(QPLCreatedByUserId))]
        public int QPLCreatedByUserId { get; set; }

        [Display(Name = nameof(QPLCreatedDate))]
        [DataMember(Name = nameof(QPLCreatedDate))]
        public DateTime QPLCreatedDate { get; set; }

        [Display(Name = nameof(QPLLastUpdatedByUserId))]
        [DataMember(Name = nameof(QPLLastUpdatedByUserId))]
        public int QPLLastUpdatedByUserId { get; set; }

        [Display(Name = nameof(QPLLastUpdatedDate))]
        [DataMember(Name = nameof(QPLLastUpdatedDate))]
        public DateTime QPLLastUpdatedDate { get; set; }

        [Display(Name = nameof(QPLExpiresDate))]
        [DataMember(Name = nameof(QPLExpiresDate))]
        public DateTime? QPLExpiresDate { get; set; }
    }
}