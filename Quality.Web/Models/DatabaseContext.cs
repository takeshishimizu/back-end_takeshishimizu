﻿using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace Quality.Web.Models
{
    public abstract class BaseContext
    {
        public abstract IDbConnection GetConnection();
        public abstract IDbCommand GetCommand(string cmdText);
    }

    public class DatabaseContext : BaseContext
    {
        private static string GetConnectionString()
        {
            var connectionString = WebConfigurationManager.ConnectionStrings["Quality.Database"]?.ConnectionString;
            // ToDo: Remove hard coded connection string once WebConfigurationManager returns ConnectionStrings
            connectionString = "Data Source=localhost;Initial Catalog=Quality.Database;Integrated Security=True";
            return connectionString;
        }

        public override IDbConnection GetConnection()
        {
            var conn = new SqlConnection(GetConnectionString());
            conn.Open();
            return conn;
        }

        public override IDbCommand GetCommand(string sqlCmdText)
        {
            return new SqlCommand(sqlCmdText, GetConnection() as SqlConnection);
        }
    }
}