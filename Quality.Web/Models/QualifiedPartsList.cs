﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Quality.Web.Models
{
    [CollectionDataContract(Name = "QualifiedPart{0}")]
    public class QualifiedPartsList
    {
        private readonly BaseContext _context;
        private QualifiedPart[] _list;

        public QualifiedPartsList(BaseContext context)
        {
            _context = context;
        }
        public QualifiedPart[] QualifiedParts
        {
            get
            {
                if (_list != null) return _list;
                using (var command = _context.GetCommand(SqlQueries.QualifiedPartsListQuery))
                {
                    var results = new List<QualifiedPart>();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            results.Add(new QualifiedPart(reader));
                        }
                    }
                    _list = results.ToArray();
                }
                return _list;
            }
        }
    }
}