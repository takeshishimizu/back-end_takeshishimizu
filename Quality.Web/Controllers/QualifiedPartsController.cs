﻿using System;
using Quality.Web.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace Quality.Web.Controllers
{
    public class QualifiedPartsController : ApiController
    {
        private readonly QualifiedPartsList _qualifiedParts = new QualifiedPartsList(new DatabaseContext());

        // GET api/<controller>
        public IEnumerable<QualifiedPart> Get()
        {
            return _qualifiedParts.QualifiedParts;
        }

        // GET api/<controller>/5
        public QualifiedPart Get(int id)
        {
            throw new NotImplementedException();
        }

        // POST api/<controller>
        public void Post([FromBody]QualifiedPart value)
        {
            throw new NotImplementedException();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]QualifiedPart value)
        {
            throw new NotImplementedException();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}